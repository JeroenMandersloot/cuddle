from itertools import islice
from typing import Iterator, Any, Union, List, Generator, Tuple

import tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.layers import BatchNormalization, MaxPool2D, Conv2D, \
    Input, Dense, Flatten
from tensorflow_core.python.training.adam import AdamOptimizer

from cuddle.data import Dataset
from cuddle.models import Model


class DummyModelSimple(Model):
    def __init__(self):
        layers = Sequential([
            Conv2D(64, (3, 3), padding='same', activation='relu'),
            MaxPool2D(strides=(2, 2), padding='valid'),
            BatchNormalization(),
            Flatten(),
            Dense(10, activation='softmax')])

        inputs = Input(shape=(10, 20, 3))
        outputs = layers(inputs)
        self._model = tf.keras.Model(inputs, outputs)
        super().__init__(output_dir='scratch')

    def keras(self) -> tf.keras.Model:
        return self._model

    def compute_loss(self,
                     targets: tf.Tensor,
                     predictions: tf.Tensor) -> tf.Tensor:
        return tf.reduce_mean(
            tf.losses.categorical_crossentropy(targets, predictions))


class DummyModelComplex(Model):
    def __init__(self):
        layers = Sequential([
            Conv2D(64, (3, 3), padding='same', activation='relu'),
            MaxPool2D(strides=(2, 2), padding='valid'),
            BatchNormalization(),
            Flatten()
        ])

        input_1 = Input(shape=(10, 20, 3))
        input_2 = Input(shape=(30, 40, 3))
        output_1 = Dense(10, activation='softmax')(layers(input_1))
        output_2 = Dense(20, activation='softmax')(layers(input_2))

        self._model = tf.keras.Model([input_1, input_2], [output_1, output_2])
        super().__init__(output_dir='scratch')

    def keras(self) -> tf.keras.Model:
        return self._model

    def compute_loss(self, targets: tf.Tensor,
                     predictions: tf.Tensor) -> tf.Tensor:
        return tf.reduce_mean(
            tf.losses.categorical_crossentropy(targets, predictions))


class DummyDatasetSimple(Dataset):
    def batches(self, batch_size: int) -> Generator[
        Tuple[Union[tf.Tensor, List[tf.Tensor]],
              Union[tf.Tensor, List[tf.Tensor]]], None, None]:
        data = iter(self)
        batch = list(islice(data, batch_size))
        while len(batch) == batch_size:
            yield self.create_batch(batch)
            batch = list(islice(data, batch_size))

    def get_input(self, x) -> Union[tf.Tensor, List[tf.Tensor]]:
        return tf.zeros(shape=(10, 20, 3))

    def get_target(self, x) -> Union[tf.Tensor, List[tf.Tensor]]:
        return tf.zeros(shape=(10,))

    def __len__(self) -> int:
        return 32

    def __iter__(self) -> Iterator[Any]:
        for i in range(len(self)):
            yield None


def test_model_1():
    model = DummyModelSimple()
    assert model.input_shape == (None, 10, 20, 3)
    assert model.output_shape == (None, 10,)


def test_model_2():
    model = DummyModelComplex()
    assert model.input_shape == [(None, 10, 20, 3), (None, 30, 40, 3)]
    assert model.output_shape == [(None, 10), (None, 20)]


def test_event_step_complete():
    model = DummyModelSimple()

    @model.on_step(interval=1)
    def cb(loss, x, y, predictions):
        assert tf.is_tensor(loss)
        assert loss.shape == ()
        assert tf.is_tensor(x)
        assert x.shape == (32, 10, 20, 3)
        assert tf.is_tensor(y)
        assert y.shape == (32, 10)
        assert tf.is_tensor(predictions)
        assert predictions.shape == (32, 10)

    dataset = DummyDatasetSimple()
    model.train(dataset=dataset,
                batch_size=len(dataset),
                num_epochs=1,
                optimizer=AdamOptimizer(learning_rate=0))


def test_event_epoch_complete():
    model = DummyModelSimple()

    @model.on_epoch(interval=1)
    def cb(epoch):
        assert epoch == 0

    dataset = DummyDatasetSimple()
    model.train(dataset=dataset,
                batch_size=len(dataset),
                num_epochs=1,
                optimizer=AdamOptimizer(learning_rate=0))
