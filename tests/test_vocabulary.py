from cuddle.data.vocabulary import Vocabulary

vocabulary = Vocabulary('abc')


def test_one_hot_1():
    one_hot = vocabulary.ohe('aabbcc')

    # Happy flow
    assert (one_hot.shape == (6, 7))  # 7 to account for special tokens.
    assert (one_hot[0, 0] == 1)
    assert (one_hot[1, 0] == 1)
    assert (one_hot[2, 1] == 1)
    assert (one_hot[3, 1] == 1)
    assert (one_hot[4, 2] == 1)
    assert (one_hot[5, 2] == 1)

    # Sad flow
    assert (one_hot[0, 1] == 0)
    assert (one_hot[0, 2] == 0)
    assert (one_hot[0, 3] == 0)
    assert (one_hot[0, 4] == 0)
    assert (one_hot[0, 5] == 0)


def test_one_hot_2():
    one_hot = vocabulary.ohe('abc', add_start=True)

    assert (one_hot.shape == (4, 7))  # 7 to account for special tokens.
    assert (one_hot[0, vocabulary[vocabulary.START_TOKEN]] == 1)
    assert (one_hot[1, 0] == 1)
    assert (one_hot[2, 1] == 1)
    assert (one_hot[3, 2] == 1)


def test_one_hot_3():
    one_hot = vocabulary.ohe('abc', length=2)

    assert (one_hot.shape == (2, 7))  # 7 to account for special tokens.
    assert (one_hot[0, 0] == 1)
    assert (one_hot[1, 1] == 1)


def test_one_hot_4():
    one_hot = vocabulary.ohe('abc', length=4)

    assert (one_hot.shape == (4, 7))  # 7 to account for special tokens.
    assert (one_hot[0, 0] == 1)
    assert (one_hot[1, 1] == 1)
    assert (one_hot[2, 2] == 1)
    assert (one_hot[3, vocabulary[vocabulary.PAD_TOKEN]] == 1)


def test_one_hot_5():
    one_hot = vocabulary.ohe('abc', add_start=True, length=3)

    assert (one_hot.shape == (3, 7))  # 7 to account for special tokens.
    assert (one_hot[0, vocabulary[vocabulary.START_TOKEN]] == 1)
    assert (one_hot[1, 0] == 1)
    assert (one_hot[2, 1] == 1)


def test_one_hot_6():
    one_hot = vocabulary.ohe('abc', add_end=True, length=3)

    assert (one_hot.shape == (3, 7))  # 7 to account for special tokens.
    assert (one_hot[0, 0] == 1)
    assert (one_hot[1, 1] == 1)
    assert (one_hot[2, vocabulary[vocabulary.END_TOKEN]] == 1)


def test_one_hot_7():
    one_hot = vocabulary.ohe('abc', add_start=True, add_end=True, length=3)

    assert (one_hot.shape == (3, 7))  # 7 to account for special tokens.
    assert (one_hot[0, vocabulary[vocabulary.START_TOKEN]] == 1)
    assert (one_hot[1, 0] == 1)
    assert (one_hot[2, vocabulary[vocabulary.END_TOKEN]] == 1)


def test_one_hot_8():
    one_hot = vocabulary.ohe('abc', add_end=True, length=5)

    assert (one_hot.shape == (5, 7))  # 7 to account for special tokens.
    assert (one_hot[0, 0] == 1)
    assert (one_hot[1, 1] == 1)
    assert (one_hot[2, 2] == 1)
    assert (one_hot[3, vocabulary[vocabulary.END_TOKEN]] == 1)
    assert (one_hot[4, vocabulary[vocabulary.PAD_TOKEN]] == 1)
