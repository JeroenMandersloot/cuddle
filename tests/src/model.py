import tensorflow as tf
from tensorflow.python.keras.layers import (
    Dense, Conv2D, MaxPool2D, BatchNormalization, Dropout, Flatten)

from tests.src.data import dummy

gpu_devices = tf.config.experimental.list_physical_devices('GPU')
for device in gpu_devices:
    tf.config.experimental.set_memory_growth(device, True)


def create_cnn_model():
    return tf.keras.Sequential([
        Conv2D(64, (3, 3), padding='same', activation='relu'),
        MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same'),
        BatchNormalization(),
        Conv2D(128, (3, 3), padding='same', activation='relu'),
        MaxPool2D(pool_size=(2, 2), strides=(2, 2), padding='same'),
        BatchNormalization(),
        Dropout(rate=0.5),
        Flatten(),
        Dense(units=2, activation='softmax')
    ])


if __name__ == '__main__':
    model = create_cnn_model()
    optimizer = tf.optimizers.Adam(learning_rate=1e-4)
    model.compile(optimizer, loss='binary_crossentropy')
    model.fit(*dummy(shape=(2, 28, 28, 1)), batch_size=2, verbose=0)
    tf.keras.backend.set_learning_phase(0)
    tf.saved_model.save(model, '/home/jeroen/Projects/deeplearning/repo/cuddle/tests/resources/tf/test_saved_model/aap')
