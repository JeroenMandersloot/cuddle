from typing import Tuple

import tensorflow as tf


def dummy(shape: Tuple[int, int, int, int]):
    xs = tf.random.normal(shape)
    ys = tf.round(tf.nn.softmax(tf.random.uniform((shape[0], 2))))
    return xs, ys
