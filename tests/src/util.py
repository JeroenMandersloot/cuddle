import os
import shutil


def get_tests_path(relative_path: str) -> str:
    """
    Takes a resource path relative to the root `tests` directory and returns
    an absolute path to it.

    :param relative_path: str
    :return: str
    """
    tests_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(tests_path, relative_path)


def scratch_dir(path: str):
    """
    Creates a temporary directory that can be used in a test and is removed
    after the test concludes. The path to this temporary directory is yielded
    so the test knows where to find it.
    """
    if not os.path.isabs(path):
        path = get_tests_path(path)
    try:
        if not os.path.exists(path):
            os.mkdir(path)
        yield path
    finally:
        shutil.rmtree(path, ignore_errors=True)
