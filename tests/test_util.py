from cuddle.util import exponential_moving_average


def test_exponential_moving_average_bias_correction():
    average = exponential_moving_average(values=[0.8, 0.7, 0.6, 0.7],
                                         beta=0.9,
                                         bias_correction=True)
    assert average[0] == 0.8
    assert 0.747 <= average[1] <= 0.748
    assert 0.692 <= average[2] <= 0.693
    assert 0.695 <= average[3] <= 0.696


def test_exponential_moving_average_no_bias_correction_initial_value_zero():
    average = exponential_moving_average(values=[0.8, 0.7, 0.6, 0.7],
                                         beta=0.9,
                                         bias_correction=False,
                                         initial_value=0)
    assert 0.079 <= average[0] <= 0.081
    assert 0.141 <= average[1] <= 0.143


def test_exponential_moving_average_no_bias_correction_initial_value_first():
    average = exponential_moving_average(values=[0.8, 0.7, 0.6, 0.7],
                                         beta=0.9,
                                         bias_correction=False,
                                         initial_value=0.8)
    assert average[0] == 0.8
    assert average[1] == 0.79
    assert average[2] == 0.771
    assert average[3] == 0.7639
