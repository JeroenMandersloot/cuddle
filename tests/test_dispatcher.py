from enum import auto

from cuddle.events import Dispatcher
from cuddle.events.event import Event


class DummyEvent(Event):
    DUMMY = auto()


def test_simple_args_and_kwargs():
    dispatcher = Dispatcher()

    def cb(*args, **kwargs):
        assert args[0] == 1
        assert kwargs['b'] == 2

    dispatcher.on(DummyEvent.DUMMY, cb)
    dispatcher.trigger(DummyEvent.DUMMY, 1, b=2)


def test_named_args():
    dispatcher = Dispatcher()

    def cb(a, b):
        assert a == 1
        assert b == 2

    dispatcher.on(DummyEvent.DUMMY, cb)
    dispatcher.trigger(DummyEvent.DUMMY, 1, b=2)


def test_reversed_named_args():
    dispatcher = Dispatcher()

    def cb(b, a):
        assert a == 1
        assert b == 2

    dispatcher.on(DummyEvent.DUMMY, cb)
    dispatcher.trigger(DummyEvent.DUMMY, a=1, b=2)
