import random

from cuddle.optimization import GridSearch


def train(a, b, c, d):
    return random.random()


def test_random_search():
    params = {
        'a': [1, 2, 3],
        'b': [1, 2],
        'c': [4, 5, 6],
        'd': [6, 7]
    }

    gs = GridSearch(train, params)
    results = gs.search()
    gs.write_results(results)
