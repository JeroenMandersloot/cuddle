import tensorflow as tf

from cuddle.data import Dataset, MergedDataset


class Dummy(Dataset):
    def __init__(self, num_instances):
        self.data = list(range(num_instances))

    def batches(self, batch_size):
        for batch in self.data[::batch_size]:
            yield self.create_batch(batch)

    def get_input(self, x):
        return tf.constant(x)

    def get_target(self, x):
        return tf.constant(x)

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return iter(self.data)


class DummyA(Dummy):
    pass


class DummyB(Dummy):
    pass


def test_alternate():
    dummy1 = DummyA(10)
    dummy2 = DummyB(30)
    merged = MergedDataset(datasets=[dummy1, dummy2], alternate=True)
    assert len(merged) == 40

    data = list(merged)
    assert len(data) == 40


def test_without_alternate():
    dummy1 = DummyA(10)
    dummy2 = DummyB(30)
    merged = MergedDataset(datasets=[dummy1, dummy2], alternate=False)
    assert len(merged) == 40

    data = list(merged)
    assert len(data) == 40
    assert all(map(lambda d: isinstance(d, DummyA), list(zip(*data))[0][:10]))
    assert all(map(lambda d: isinstance(d, DummyB), list(zip(*data))[0][10:]))
