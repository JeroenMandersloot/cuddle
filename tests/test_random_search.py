import random

from cuddle.optimization import RandomSearch


def train(a, b, c, d):
    return random.random()


def test_random_search():
    params = {
        'a': random.random,
        'b': random.random,
        'c': random.random,
        'd': random.random,
    }

    rs = RandomSearch(10, train, params)
    results = rs.search()
    rs.write_results(results)
