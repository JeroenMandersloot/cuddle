import pytest
import tensorflow as tf
from tensorflow.python.eager.def_function import Function
from tensorflow.python.eager.function import ConcreteFunction


@pytest.fixture()
def fizzbuzz():
    @tf.function
    def _fizzbuzz(x):
        response = ''
        if x % 3 == 0:
            response += 'fizz'
        if x % 5 == 0:
            response += 'buzz'
        return response or tf.as_string(x)

    return _fizzbuzz


@pytest.fixture()
def fizzbuzz_with_signature():
    @tf.function(input_signature=[tf.TensorSpec([], tf.int32)])
    def _fizzbuzz_with_signature(x):
        response = ''
        if x % 3 == 0:
            response += 'fizz'
        if x % 5 == 0:
            response += 'buzz'
        return response or tf.as_string(x)

    return _fizzbuzz_with_signature


def test_tf_function_type(fizzbuzz):
    assert isinstance(fizzbuzz, Function)


def test_tf_function_with_signature_type(fizzbuzz_with_signature):
    assert isinstance(fizzbuzz_with_signature, Function)


def test_tf_concrete_function_type(fizzbuzz):
    assert isinstance(fizzbuzz.get_concrete_function(1), ConcreteFunction)


def test_tf_function_call(fizzbuzz):
    """
    The first time a tf.function annotated method is called, the graph is
    created and executed. Subsequent calls use the graph created earlier.
    """
    assert not hasattr(fizzbuzz, '_concrete_stateful_fn')
    assert fizzbuzz(15) == 'fizzbuzz'
    assert hasattr(fizzbuzz, '_concrete_stateful_fn')


def test_concrete_functions_with_python_numerical_arguments(fizzbuzz):
    """
    When you get the concrete function of a Tensorflow Function that accepts
    one argument and pass along a Python numerical argument, the resulting
    graph will be instantiated with that argument as its fixed input. Thus,
    every time you get the concrete function and pass a different Python
    numerical value as an argument, a different graph will be instantiated
    and it won't take any inputs.
    """
    f1 = fizzbuzz.get_concrete_function(1)
    f2 = fizzbuzz.get_concrete_function(2)
    assert f1 is not f2  # The two graphs will be different.
    assert not f1.inputs


def test_concrete_functions_with_same_tensor_dtype(fizzbuzz):
    """
    When you get the concrete function of a Tensorflow Function that accepts
    one argument and pass along a Tensorflow tensor argument, the shape and
    dtype of that tensor will be used to "trace" the input that the resulting
    graph will accept. If a graph with the same input tensor shape and dtype
    as one that is being requested already exists, no new graph is
    instantiated and the existing graph is returned instead.
    """
    f1 = fizzbuzz.get_concrete_function(tf.constant(1))
    f2 = fizzbuzz.get_concrete_function(tf.constant(2))
    assert f1 is f2
    assert len(f1.inputs) == 1


def test_concrete_functions_with_same_tensor_spec(fizzbuzz):
    """
    The same goes for passing along TensorSpecs to `get_concrete_function`.
    """
    f1 = fizzbuzz.get_concrete_function(tf.TensorSpec([], tf.int32))
    f2 = fizzbuzz.get_concrete_function(tf.TensorSpec([], tf.int32))
    assert f1 is f2


def test_concrete_functions_with_tensor_spec_and_matching_tensor(fizzbuzz):
    """
    However, when you mix TensorSpecs and tensors, the resulting graphs are
    not the same.
    """
    f1 = fizzbuzz.get_concrete_function(tf.constant(1))
    f2 = fizzbuzz.get_concrete_function(tf.TensorSpec([], tf.int32))
    assert f1 is not f2


def test_concrete_function_with_signature(fizzbuzz_with_signature):
    """
    If an input signature was already specified in the `@tf.function()`
    decorator, we don't have to pass any arguments to get the concrete
    function. Also, if we call `get_concrete_function()` multiple times, we'll
    get back the same graph each time.
    """
    f1 = fizzbuzz_with_signature.get_concrete_function()
    f2 = fizzbuzz_with_signature.get_concrete_function()
    assert f1 is f2
    assert len(f1.inputs) == 1
