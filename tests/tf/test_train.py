import os

import numpy as np
import pytest
import tensorflow as tf

from tests.src.data import dummy
from tests.src.model import create_cnn_model
from tests.src.util import scratch_dir


@pytest.fixture()
def model():
    yield create_cnn_model()


@pytest.fixture(scope='module')
def trained_model():
    """
    The behaviour for trained and untrained models is different, so we want to
    be able to test these separately. This fixture yields a model trained on
    some dummy data.
    """
    model = create_cnn_model()
    optimizer = tf.optimizers.Adam(learning_rate=1e-4)
    model.compile(optimizer, loss='binary_crossentropy')
    model.fit(*dummy(shape=(2, 28, 28, 1)), batch_size=2, verbose=0)
    yield model


@pytest.fixture()
def scratch():
    yield from scratch_dir('scratch/train')


def test_directory_structure_one_save(model: tf.keras.Model, scratch: str):
    checkpoint = tf.train.Checkpoint(model=model)
    checkpoint.save(file_prefix=os.path.join(scratch, 'ckpt'))
    expected = {'ckpt-1.data-00000-of-00001', 'checkpoint', 'ckpt-1.index'}
    contents = set(os.listdir(scratch))
    assert expected == contents


def test_directory_structure_two_saves(model: tf.keras.Model, scratch: str):
    checkpoint = tf.train.Checkpoint(model=model)
    checkpoint.save(file_prefix=os.path.join(scratch, 'ckpt'))
    checkpoint.save(file_prefix=os.path.join(scratch, 'ckpt'))
    expected = {
        'ckpt-1.data-00000-of-00001',
        'ckpt-1.index',
        'ckpt-2.data-00000-of-00001',
        'ckpt-2.index',
        'checkpoint'
    }
    contents = set(os.listdir(scratch))
    assert expected == contents


def test_checkpoint_restore(trained_model: tf.keras.Model, scratch: str):
    checkpoint = tf.train.Checkpoint(model=trained_model)
    checkpoint.save(file_prefix=os.path.join(scratch, 'ckpt'))
    weight = trained_model.weights[0]
    original_value = weight.numpy()
    weight.assign(np.zeros(weight.shape))
    assert not np.all(weight.numpy() == original_value)
    checkpoint.restore(tf.train.latest_checkpoint(scratch))
    assert np.all(weight.numpy() == original_value)


def test_aap(trained_model, scratch):
    weights_path = os.path.join(scratch, 'weights')
    trained_model.save_weights(weights_path, save_format='tf')
    pass

# TODO: test checkpoint vs keras.save_weights?
