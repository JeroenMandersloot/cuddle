import os

import pytest
import tensorflow as tf

from tests.src.data import dummy
from tests.src.model import create_cnn_model
from tests.src.util import get_tests_path, scratch_dir


@pytest.fixture()
def model():
    yield create_cnn_model()


@pytest.fixture(scope='module')
def trained_model():
    """
    The behaviour for trained and untrained models is different, so we want to
    be able to test these separately. This fixture yields a model trained on
    some dummy data.
    """
    model = create_cnn_model()
    optimizer = tf.optimizers.Adam(learning_rate=1e-4)
    model.compile(optimizer, loss='binary_crossentropy')
    model.fit(*dummy(shape=(2, 28, 28, 1)), batch_size=2, verbose=0)
    yield model


@pytest.fixture()
def scratch():
    yield from scratch_dir('scratch/saved_model')


@pytest.fixture()
def saved_model_path():
    yield get_tests_path('resources/tf/test_saved_model/cnn_model')


def test_directory_structure(model: tf.keras.Model, scratch: str):
    tf.saved_model.save(model, scratch)
    expected = {'variables', 'assets', 'saved_model.pb'}
    contents = set(os.listdir(scratch))
    assert expected == contents


def test_load_saved_model(saved_model_path):
    model = tf.saved_model.load(saved_model_path)
    assert len(model.variables) == 14
    assert len(model.trainable_variables) == 10
    assert len(list(filter(lambda var: var.trainable, model.variables))) == 10
    assert len(model.signatures) == 1
    assert 'serving_default' in model.signatures


def test_save_model_with_custom_signature(model: tf.keras.Model, scratch: str):
    """
    By providing a custom signature you can "override" the default behaviour
    of the model that is being saved. In this method, for example, even though
    we save a CNN model we override it with a simple graph that simply computes
    the square of its input.

    :param model: tf.keras.Model
    :param scratch: str
    """

    @tf.function(input_signature=[tf.TensorSpec([], tf.float32)])
    def square(x):
        return x * x

    tf.saved_model.save(model, scratch, signatures=square)
    loaded_model = tf.saved_model.load(scratch)
    outputs = loaded_model.signatures['serving_default'](tf.constant(2.))
    assert isinstance(outputs, dict)
    assert outputs['output_0'] == 4.


def test_save_model_with_multiple_signatures(trained_model: tf.keras.Model,
                                             scratch: str):
    """
    If you want to save multiple signatures, the easiest way would be to define
    a custom tf.Module, the top-level class for a Tensorflow object (such as a
    tf.Variable or tf.keras.Model). The idea is to create a separate method for
    each signature and decorate it with `tf.function`. If we then save this
    custom tf.Module instead of the underlying tf.keras.Model we will have
    access to all signatures.

    :param trained_model: tf.keras.Model
    :param scratch: str
    """

    class SignatureWrapper(tf.Module):
        def __init__(self, model):
            self.model = model
            super().__init__('signature_wrapper')

        @tf.function(
            input_signature=[tf.TensorSpec([None, 28, 28, 1], tf.float32)])
        def __call__(self, inputs):
            return self.model(inputs)

        @tf.function(input_signature=[tf.TensorSpec([], tf.float32)])
        def square(self, x):
            return x * x

    module = SignatureWrapper(trained_model)
    tf.saved_model.save(
        module,
        scratch,
        signatures={
            'serving_default': module.__call__,
            'square': module.square
        }
    )
    loaded_model = tf.saved_model.load(scratch)
    outputs = loaded_model.signatures['square'](tf.constant(2.))
    assert isinstance(outputs, dict)
    assert outputs['output_0'] == 4.

    dummy_input, _ = dummy(shape=(1, 28, 28, 1))
    outputs = loaded_model.signatures['serving_default'](dummy_input)
    assert isinstance(outputs, dict)
    assert outputs['output_0'].shape == (1, 2)


def test_save_custom_method_as_module(scratch: str):
    class Square(tf.Module):
        @tf.function(input_signature=[tf.TensorSpec([], tf.float32, name='x')])
        def __call__(self, x):
            return x * x

    tf.saved_model.save(
        Square(),
        scratch,
    )
    loaded_model = tf.saved_model.load(scratch)
    outputs = loaded_model.signatures['serving_default'](tf.constant(2.))
    assert outputs['output_0'] == 4.
