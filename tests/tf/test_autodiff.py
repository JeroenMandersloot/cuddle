import tensorflow as tf


def test_gradient_tape():
    """
    A simple working example of TensorFlow's GradientTape.
    """
    x = tf.constant(1.0)
    with tf.GradientTape() as tape:
        tape.watch(x)
        y = x * x
    g = tape.gradient(y, x)
    assert g.dtype.is_floating
    assert g == 2.0


def test_gradient_tape_with_integer_returns_none():
    """
    TensorFlow's GradientTape only works with floating tensors. The
    `GradientTape.watch()` method logs a warning when a non-floating Tensor
    is passed as an argument, but I think it would be better to throw an error
    here. This silent behaviour could result in hard-to-find bugs.
    """
    x = tf.constant(1)
    with tf.GradientTape() as tape:
        tape.watch(x)
        y = x * x
    g = tape.gradient(y, x)
    assert x.dtype.is_integer
    assert g is None


def test_gradient_tape_automatically_watches_trainable_variables():
    x1 = tf.Variable(1., trainable=True)
    x2 = tf.Variable(1., trainable=False)
    with tf.GradientTape() as tape:
        y = x1 * x1 + x2 * x2
    g1, g2 = tape.gradient(y, [x1, x2])
    assert g1 == 2.0
    assert g2 is None  # `x2` is not trainable and so will not be watched
