import tensorflow as tf


def test_tensor_is_immutable():
    tensor = tf.range(10)
    assert tensor[0] == 0
    try:
        tensor[0] = 1
        assert False
    except TypeError:
        assert True
