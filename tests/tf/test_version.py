import tensorflow as tf


def test_version():
    assert tf.version.VERSION == '2.0.0'
