import os

import tensorflow as tf
from tensorflow.python.data import TextLineDataset
from tensorflow.python.data.ops.dataset_ops import TensorSliceDataset

from tests.src.util import get_tests_path


def test_from_tensor_slices_1d():
    xs = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4])
    assert isinstance(xs, TensorSliceDataset)
    assert all(map(lambda x: x.shape == (), xs))
    assert len(list(xs)) == 4


def test_from_tensor_slices_2d():
    xs = tf.data.Dataset.from_tensor_slices([[1, 2, 3, 4], [5, 6, 7, 8]])
    assert isinstance(xs, TensorSliceDataset)
    assert all(map(lambda x: x.shape == (4,), xs))
    assert len(list(xs)) == 2


def test_from_tensor_slices_batches():
    xs = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4])
    batches = list(xs.batch(2))
    assert len(batches) == 2
    assert all(map(lambda x: x.shape == (2,), batches))


def test_from_tensor_slices_batches_unequal_size():
    xs = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4, 5])
    batches = list(xs.batch(2))
    assert len(batches) == 3
    assert batches[2].shape == (1,)


def test_from_tensor_slices_batches_unequal_size_with_drop():
    xs = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4, 5])
    batches = list(xs.batch(2, drop_remainder=True))
    assert len(batches) == 2
    assert all(map(lambda x: x.shape == (2,), batches))


def test_from_tensor_slices_instantiate_class_directly():
    xs = TensorSliceDataset([1, 2, 3, 4])
    assert all(map(lambda x: x.shape == (), xs))
    assert len(list(xs)) == 4


def test_text_line_dataset():
    files = list(map(get_tests_path, [
        'resources/tf/test_data/text_line_dataset_1.txt',
        'resources/tf/test_data/text_line_dataset_2.txt'
    ]))
    xs = TextLineDataset(files)
    assert len(list(xs)) == 3
    assert all(map(lambda x: x.dtype == tf.string, xs))


def test_files_dataset():
    root = get_tests_path('resources/tf/test_data')
    xs = tf.data.Dataset.list_files(file_pattern=os.path.join(root, '*.txt'))
    files = set(x.numpy().decode('utf-8') for x in xs)
    expected = set(
        os.path.join(root, f) for f in os.listdir(root) if f.endswith('.txt'))
    assert expected == files


def test_map():
    xs = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4])
    expected = list(tf.data.Dataset.from_tensor_slices([2, 3, 4, 5]))
    mapped = list(xs.map(lambda x: x + 1))
    assert expected == mapped


def test_batch_correctly_stacks_tensors():
    xs = tf.data.Dataset.from_tensor_slices([1, 2, 3, 4])
    xs = xs.map(lambda x: (x, tf.one_hot(x, depth=4))).batch(2)
    idx, one_hot = next(iter(xs))
    assert idx.shape == (2,)
    assert one_hot.shape == (2, 4)
