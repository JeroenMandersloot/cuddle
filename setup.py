import os

from setuptools import setup

with open('README.md') as f:
    long_description = f.read()

with open('requirements.txt') as f:
    requirements = f.readlines()

packages = [f'cuddle.{folder}' for folder in os.listdir('cuddle')
            if os.path.isdir(os.path.join('cuddle', folder))
            and not folder.startswith('_')]

setup(
    name='Cuddle',
    version='0.1.0',
    packages=packages,
    long_description=long_description,
    install_requires=requirements,
)
