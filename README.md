## Why cuddle?

Cuddle (short for **C**onsistent, **U**nified **D**ata and **D**eep **L**earning 
**E**xtension) is a thin, lightweight package that can be used alongside 
TensorFlow and Keras to better streamline your data processing and deep learning 
pipelines.  

TensorFlow gives you fine-grained control over your network architecture. For 
people who prefer to know exactly what's going on under the hood this a big 
advantage over Keras. However, TensorFlow has a rather bloated API that often 
offers multiple ways of essentially doing the same thing (e.g. saving a model, 
loading a dataset, training a model, etc).

Keras on the other hand, while very user-friendly, sometimes tries to do too 
much for you. For example, it's easy to forget to tell your model when it should 
run in training mode versus inference mode, which is important for like Dropout 
and BatchNormalization layers.  

Cuddle is meant to combine the best of both worlds by offering up a thin `Model`
class in which you are free to specify the model details in either TensorFlow or
Keras. Cuddle standardizes common tasks such as saving models, in the spirit of 
the user-friendly API of Keras. Meanwhile, it leaves implementing most of the 
technical details to you, giving you the fine-grained control of TensorFlow. 

In addition, Cuddle forces you to be explicit about things that should not be 
left implicit. For example, every call to a `Model.predict` forces you to pass 
along a boolean value for the `training` argument.

## Examples

