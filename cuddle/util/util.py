from typing import List


def exponential_moving_average(values: List[float],
                               beta: float,
                               bias_correction: bool = True,
                               initial_value: float = 0) -> List[float]:
    """
    Takes a list of float values and returns a list of equal length,
    representing an exponential moving average at each time step.

    :param values: List[float]
    :param beta: float
    :param bias_correction: bool
    :param initial_value: float
    :return: List[float]
    """
    v = initial_value
    average = []
    for t, c in enumerate(values):
        v = beta * v + (1 - beta) * c
        average.append(v)
    if bias_correction:
        return [v / (1 - beta ** (t + 1)) for t, v in enumerate(average)]
    return average
