import abc
from typing import Generator, Tuple, Any, Iterator, List, Union

import tensorflow as tf


class Dataset:
    @abc.abstractmethod
    def batches(self, batch_size: int) -> Generator[
        Tuple[Union[tf.Tensor, List[tf.Tensor]],
              Union[tf.Tensor, List[tf.Tensor]]], None, None]:
        """
        A generator that yields batches of size `batch_size`.

        :param batch_size: int, the batch size
        :return: Generator[
            Tuple[Union[tf.Tensor, List[tf.Tensor]],
                  Union[tf.Tensor, List[tf.Tensor]]], None, None]
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_input(self, x) -> Union[tf.Tensor, List[tf.Tensor]]:
        """
        Converts an instance `x` to a tensor that can be used directly by a
        TensorFlow model. If the model expects multiple inputs, this method
        should return a list of tensors of the same size as the model's input.

        :param x: Any
        :return: Union[tf.Tensor, List[tf.Tensor]]
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_target(self, x) -> Union[tf.Tensor, List[tf.Tensor]]:
        """
        Returns the target value for instance `x` as a tensor. The target value
        can in principle be anything, e.g. one-hot encoded for classification,
        continuous-valued for regression, or empty for unsupervised. Proper
        handling of the return value is left to the outer scope.

        If the model returns multiple inputs, this method should return a list
        of tensors of the same size as the model's output.

        :param x: Any
        :return: Union[tf.Tensor, List[tf.Tensor]]
        """
        raise NotImplementedError

    def create_batch(self, xs: List[Any]) -> Tuple[
        Union[tf.Tensor, List[tf.Tensor]],
        Union[tf.Tensor, List[tf.Tensor]]
    ]:
        """
        Creates a TensorFlow-ready batch from the instances in `xs`.

        :param xs: List[Any]
        :return: Tuple[Union[tf.Tensor, List[tf.Tensor]],
                       Union[tf.Tensor, List[tf.Tensor]]]
        """
        inputs = [self.get_input(x) for x in xs]
        targets = [self.get_target(x) for x in xs]
        return self.flatten_tensors(inputs), self.flatten_tensors(targets)

    @staticmethod
    def flatten_tensors(
            tensors: Union[List[tf.Tensor], List[List[tf.Tensor]]]
    ) -> Union[tf.Tensor, List[tf.Tensor]]:
        """
        Converts a list of tensors to a stacked tensor with an extra dimension.
        If `tensors` is a nested list of tensors, each inner list of tensors is
        converted to a single stacked tensor and a list of stacked tensors is
        returned.

        :param tensors: Union[List[tf.Tensor], List[List[tf.Tensor]]])
        :return: Union[tf.Tensor, List[tf.Tensor]]
        """
        if not tensors:
            raise ValueError('Cannot flatten empty list of tensors.')
        if all(isinstance(ts, list) for ts in tensors):
            return list(tf.stack(ts, axis=0) for ts in zip(*tensors))
        return tf.stack(tensors, axis=0)

    @abc.abstractmethod
    def __len__(self) -> int:
        """
        Returns the size of the dataset.

        :return: int
        """
        raise NotImplementedError

    @abc.abstractmethod
    def __iter__(self) -> Iterator[Any]:
        """
        Returns an iterator over the instances in the dataset.

        :return: Iterator[Any]
        """
        raise NotImplementedError
