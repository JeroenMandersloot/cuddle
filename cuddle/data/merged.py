import random
from abc import ABC
from typing import Iterator, Any, Union, List, Tuple

import tensorflow as tf

from cuddle.data.dataset import Dataset


class MergedDataset(Dataset, ABC):
    """
    A lightweight, abstract class that can be used to easily merge multiple
    datasets together.
    """

    def __init__(self, datasets: List[Dataset], alternate: bool = False):
        """
        :param datasets: List[Dataset]
        :param alternate: bool
        """
        self.datasets = datasets
        self.alternate = alternate

    def get_input(
            self,
            x: Tuple[Dataset, Any]
    ) -> Union[tf.Tensor, List[tf.Tensor]]:
        return x[0].get_input(x[1])

    def get_target(
            self,
            x: Tuple[Dataset, Any]
    ) -> Union[tf.Tensor, List[tf.Tensor]]:
        return x[0].get_target(x[1])

    def __len__(self) -> int:
        """
        Returns the sum of the lengths of all datasets that were merged.

        :return: int
        """
        return sum(map(len, self.datasets))

    def __iter__(self) -> Iterator[Tuple[Dataset, Any]]:
        """
        Returns an iterator over all data in the datasets that were merged.

        :return: Iterator[Tuple[Dataset, Any]]
        """

        if self.alternate:
            def get_weights(ds: List[Tuple[Dataset, Iterator]]) -> List[float]:
                sizes = [len(d) for d, _ in ds]
                return [s / sum(sizes) for s in sizes]

            iterators = [(d, iter(d)) for d in self.datasets]
            weights = get_weights(iterators)
            while iterators:
                dataset, iterator = random.choices(iterators, weights)[0]
                try:
                    yield dataset, next(iterator)
                except StopIteration:
                    iterators.remove((dataset, iterator))
                    weights = get_weights(iterators)
        else:
            yield from ((d, x) for d in self.datasets for x in d)
