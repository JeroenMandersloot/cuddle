from typing import List, Iterable, Optional, Union

import tensorflow as tf


class Vocabulary:
    """
    A convenience class that makes dealing with vocabularies much easier.

    After creating a Vocabulary instance from a list of tokens (referred to
    here as an `alphabet`) you can easily convert tokens into their respective
    numeric indices and vice versa:

    ```
        vocabulary = Vocabulary('abc')
        vocabulary['a']  # -> returns 0
        vocabulary[0]    # -> returns 'a'
    ```

    Special tokens that are commonly used, (e.g. <PAD>, <SOS>, <EOS> and <UNK>)
    are combined with the `alphabet` to form the complete vocabulary. Class
    variables in subclasses ending in "_TOKEN" are automatically recognized as
    additional special tokens and are also added upon initialization.
    """

    PAD_TOKEN = '<PAD>'
    UNKNOWN_TOKEN = '<UNK>'
    START_TOKEN = '<SOS>'
    END_TOKEN = '<EOS>'

    def __init__(self, alphabet: Iterable[str]):
        """
        Creates a vocabulary from the tokens specified in `alphabet`.

        :param alphabet: Iterable[str]
        """
        t = [v for k, v in __class__.__dict__.items() if k.endswith('_TOKEN')]
        intersect = set(alphabet) & set(t)
        if intersect:
            raise ValueError(f'Alphabet contains special tokens: {intersect}')
        self.alphabet: List[str] = list(alphabet)
        self.tokens: List[str] = self.alphabet + t
        self._token_to_index = {t: i for i, t in enumerate(self.tokens)}
        self._index_to_token = {i: t for i, t in enumerate(self.tokens)}

    def get_token(self, index: int) -> str:
        """
        Returns the token matching the given `index`.

        :param index: int
        :return: str
        """
        return self._index_to_token[index]

    def get_index(self, token: str) -> int:
        """
        Returns the index matching the given `token`.

        :param token: str
        :return: int
        """
        return self._token_to_index[token]

    def ohe(self,
            text: Iterable[str],
            length: Optional[int] = None,
            add_start: bool = False,
            add_end: bool = False) -> tf.Tensor:
        """
        Converts `text` to a one-hot encoded tensor. Each element in `text` is
        iterated over and should match one of the tokens in the vocabulary.

        If `length` is specified, the resulting tensor will be padded along
        axis 0. If `text` contains more tokens than `length`, the remaining
        tokens will be truncated.

        If `add_start` or `add_end` is set to True, the `<SOS>` or `<EOS>`
        tokens are prefixed or suffixed, respectively.

        :param text: Iterable[str]
        :param length: Optional[int]
        :param add_start: bool
        :param add_end: bool
        :return: tf.Tensor, shape (num_tokens, vocab_size)
        """
        text = list(text)
        if add_start:
            text = [self.START_TOKEN] + text
        if length:
            if add_end and len(text) >= length:
                length = length - 1
            text = text[:length]
        if add_end:
            text = text + [self.END_TOKEN]
        if length and len(text) < length:
            text = (text + length * [self.PAD_TOKEN])[:length]
        return tf.one_hot([self.get_index(c) for c in text], depth=len(self))

    def ohd(self,
            one_hot: tf.Tensor,
            delimiter: Optional[str] = None,
            skip_special: bool = True) -> Union[List, str]:
        """
        Converts a one-hot encoded tensor back to the corresponding tokens. If
        a `delimiter` string is specified, the tokens are joined together and
        returned as a string instead of a list of tokens. If `skip_special` is
        True, special tokens are not included in the return value.

        :param one_hot: tf.Tensor, shape (num_tokens, vocab_size)
        :param delimiter: Optional[str]
        :param skip_special: bool
        """
        tokens = [
            self.get_token(i.numpy()) for i in tf.argmax(one_hot, axis=1)]
        if skip_special:
            tokens = [token for token in tokens if token in self.alphabet]
        if delimiter is not None:
            return delimiter.join(tokens)
        return tokens

    def __getitem__(self, item: Union[str, int]) -> Union[int, str]:
        """
        If `item` is a token, returns the matching index.
        If `item` is an index, returns the matching token.

        :param item: Union[str, int]
        :return: Union[int, str]
        """
        if isinstance(item, str):
            return self.get_index(item)
        if isinstance(item, int):
            return self.get_token(item)
        raise ValueError("Vocabulary key must be of type `str` or `int`")

    def __len__(self) -> int:
        """
        Returns the number of tokens in the vocabulary, including the special
        tokens.

        :return: int
        """
        return len(self.tokens)
