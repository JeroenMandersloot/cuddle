from collections import defaultdict
from typing import Callable, Optional

from cuddle.events.event import Event


class Dispatcher:
    def __init__(self):
        self._handlers = defaultdict(list)
        self._cache = None

    def trigger(self, event: Event, *args, **kwargs):
        """
        Trigger an `event` and call all registered handlers for that event,
        passing along the `args` and `kwargs`.

        :param event: Event, the event that was triggered
        """
        for handler in self._handlers[event]:
            handler(*args, **kwargs)

    def on(self, event: Event, handler: Callable):
        """
        Register a `handler` for an `event`.

        :param event: Event, the event to listen for
        :param handler: Callable, called when the event is triggered
        """
        self._handlers[event].append(handler)

    def remove(self, event: Event, handler: Optional[Callable] = None):
        """
        Unregister the specified `handler` from the `event`. If no `handler` is
        specified, all handlers listening for the `event` are removed.

        :param event: Event
        :param handler: Optional[Callable], the specific handler to unregister
        """
        if handler:
            self._handlers[event].remove(handler)
        elif event in self._handlers:
            del self._handlers[event]

    def __enter__(self):
        """
        When entering a context, store the current handlers in the `_cache`
        variable so we can restore the original state of this dispatcher when
        we exit the context.
        """
        self._cache = self._handlers.copy()

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Reset all handlers in this dispatcher to their state before entering
        the context.
        """
        assert self._cache is not None
        self._handlers = self._cache
