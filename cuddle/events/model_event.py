from enum import auto

from cuddle.events.event import Event


class ModelEvent(Event):
    STEP_COMPLETED = auto()
    EPOCH_COMPLETED = auto()
