import abc
import csv
import os
import pickle
from collections import defaultdict
from contextlib import contextmanager
from functools import wraps
from typing import List, Union, Tuple, Optional, Dict

import matplotlib.pyplot as plt
import tensorflow as tf
import yaml
from tensorflow.python.keras.backend import (
    set_learning_phase, learning_phase, get_session)
from tensorflow.python.training.optimizer import Optimizer
from tqdm import tqdm

from cuddle.data.dataset import Dataset
from cuddle.events.dispatcher import Dispatcher
from cuddle.events.model_event import ModelEvent
from cuddle.util import exponential_moving_average


class Model:
    @abc.abstractmethod
    def __init__(self,
                 output_dir: Optional[str],
                 dispatcher: Optional[Dispatcher] = None):
        """
        Instantiate a new Model instance.

        :param output_dir: Optional[str], directory to store checkpoints
        :param dispatcher: Optional[Dispatcher], an event handler
        """
        self.output_dir = output_dir
        if self.output_dir:
            os.makedirs(output_dir, exist_ok=True)
        self.dispatcher = dispatcher if dispatcher else Dispatcher()
        self.global_step = 0
        self.history: Dict[str, List[Tuple[int, float]]] = defaultdict(list)

    @abc.abstractmethod
    def keras(self) -> tf.keras.Model:
        """
        Returns a Keras representation of the model.

        :return: tf.keras.Model
        """
        raise NotImplementedError

    @abc.abstractmethod
    def compute_loss(
            self,
            targets: Union[tf.Tensor, List[tf.Tensor]],
            predictions: Union[tf.Tensor, List[tf.Tensor]]
    ) -> tf.Tensor:
        """
        Computes the loss corresponding to the `predictions` for the `targets`.

        :param targets: tf.Tensor
        :param predictions: tf.Tensor
        :return: tf.Tensor
        """
        raise NotImplementedError

    def predict(
            self,
            x: Union[tf.Tensor, List[tf.Tensor]],
            training: bool,
            batch_size: Optional[int] = None
    ) -> Union[tf.Tensor, List[tf.Tensor]]:
        """
        Apply the model to `x`. Specify whether the model should be run in
        training or inference mode by setting `training` to True or False,
        respectively. If `batch_size` is specified, predictions are made on
        batches of `x` at a time.

        :param x: Union[tf.Tensor, List[tf.Tensor]]
        :param training: bool
        :param batch_size: Optional[int]
        :return: Union[tf.Tensor, List[tf.Tensor]]
        """
        with self.learning_phase(training):
            # If the model should be applied to batches of `x`, some additional
            # logic is required since we have to check for all possible formats
            # `x` and the output could take (either `tf.Tensor` or
            # `List[tf.Tensor]`).
            if batch_size:
                # If `x` is a list, apply the model to a list as well.
                if isinstance(self.input_shape, list):
                    ts = [
                        self.keras()([_x[i:i + batch_size] for _x in x])
                        for i in range(0, len(x[0]), batch_size)]

                # If `x` is a `tf.Tensor`, we feed the model tensor slices.
                else:
                    ts = [self.keras()(x[i:i + batch_size])
                          for i in range(0, len(x), batch_size)]

                # Depending on whether the output is a list or `tf.Tensor`, we
                # may have to do some post-processing on the predictions.
                if isinstance(self.output_shape, list):
                    return list(map(lambda t: tf.concat(t, axis=0), ts))
                return tf.concat(ts, axis=0)

            # If no batch size is specified, we can assume `x` is already in
            # the correct format, so we can feed it to the model directly.
            return self.keras()(x)

    def train(self,
              dataset: Dataset,
              batch_size: int,
              num_epochs: int,
              optimizer: Optimizer,
              save_interval: Optional[int] = None,
              validation: Union[Dataset, Dict[str, Dataset], None] = None,
              verbose: bool = True):
        """
        Train the model on `dataset` in batches of size `batch_size` for
        `num_epochs` epochs.

        If a `save_interval` is specified, a checkpoint is stored after every
        `save_interval` steps (i.e. batches).

        If `validation` is also specified, each time the model is saved, it is
        also evaluated on the validation set. If `validation` is a dict of
        `Dataset` instances, the model is evaluated on each of the datasets
        separately. The key corresponding to each dataset determines its label
        when the model's history is plotted.

        :param dataset: Dataset
        :param batch_size: int
        :param num_epochs: int
        :param optimizer: Optimizer
        :param save_interval: Optional[int]
        :param validation: Union[Dataset, Dict[str, Dataset], None]
        :param verbose: bool
        """
        with self.dispatcher:
            # Register a temporary train step callback that takes care of all
            # administrative tasks after `save_interval` steps such as saving
            # the model, computing the validation loss(es), etc.
            if save_interval:
                self.on_step(save_interval, validation, optimizer)(
                    self._train_step_callback)

            # Start the training loop.
            for epoch in range(num_epochs):
                batches = dataset.batches(batch_size)
                if verbose:
                    total = len(dataset) // batch_size
                    batches = tqdm(batches, unit_scale=batch_size, total=total)
                for x, y in batches:
                    self.global_step += 1
                    with tf.GradientTape() as tape:
                        predictions = self.predict(x, training=True)
                        loss = self.compute_loss(y, predictions)
                    variables = self.keras().trainable_variables
                    gradients = tape.gradient(loss, variables)
                    optimizer.apply_gradients(zip(gradients, variables))
                    self.dispatcher.trigger(
                        ModelEvent.STEP_COMPLETED, loss, x, y, predictions)
                self.dispatcher.trigger(ModelEvent.EPOCH_COMPLETED, epoch)

    def validate(self, validation: Dataset, batch_size: int = 1) -> tf.Tensor:
        """
        Computes the loss on a `validation` set. By default, instances in the
        validation set are processed independently, but if desired they can be
        processed in batches by specifying a custom value for `batch_size`.

        :param validation: Dataset
        :param batch_size: int
        :return: tf.Tensor
        """
        return tf.reduce_mean([
            self.compute_loss(y, self.predict(x, training=False))
            for x, y in validation.batches(batch_size=batch_size)
        ])

    def save(self, **kwargs):
        """
        Saves the model weights and metadata in one unified format that is
        understood by the `load()` method. Additional `kwargs` to be
        stored alongside the metadata can also be passed.
        """
        if not self.output_dir:
            raise ValueError("Can't save model: no output directory specified")
        self.keras().save_weights(os.path.join(self.output_dir, 'model.h5'))
        metadata = dict(**kwargs, global_step=self.global_step)
        with open(os.path.join(self.output_dir, 'metadata.yaml'), 'w') as f:
            yaml.safe_dump(metadata, f)
        if self.history:
            history_dir = os.path.join(self.output_dir, 'history')
            os.makedirs(history_dir, exist_ok=True)
            for label, history in self.history.items():
                history_file = os.path.join(history_dir, f'{label}.tsv')
                with open(history_file, 'w') as f:
                    f.write('\n'.join(['{}\t{}'.format(*h) for h in history]))
        self.plot_history()

    def save_optimizer(self, optimizer: Optimizer):
        """
        Saves the `optimizer` internal state.

        :param optimizer: Optimizer
        """
        optimizer_path = os.path.join(self.output_dir, 'optimizer.obj')
        with open(optimizer_path, 'wb') as f:
            pickle.dump(optimizer, f)

    def load(self):
        """
        Load the model weights and metadata.
        """
        if not self.output_dir:
            raise ValueError("Can't load model: no output directory specified")
        self.keras().load_weights(os.path.join(self.output_dir, 'model.h5'))
        with open(os.path.join(self.output_dir, 'metadata.yaml'), 'r') as f:
            metadata = yaml.safe_load(f)
        self.global_step = metadata['global_step']
        history_dir = os.path.join(self.output_dir, 'history')
        if os.path.exists(history_dir):
            for filename in os.listdir(history_dir):
                label = os.path.splitext(filename)[0]
                with open(os.path.join(history_dir, filename), 'r') as f:
                    for global_step, loss in csv.reader(f, delimiter='\t'):
                        self.history[label].append(
                            (int(global_step), float(loss)))

    def load_optimizer(self) -> Optimizer:
        """
        Loads a previously stored optimizer, restoring its internal state.

        :return: Optimizer
        """
        optimizer_path = os.path.join(self.output_dir, 'optimizer.obj')
        with open(optimizer_path, 'rb') as f:
            optimizer = pickle.load(f)
        if isinstance(optimizer, Optimizer):
            return optimizer
        raise TypeError(f'Optimizer has invalid type {type(optimizer)}')

    def update_history(self, label: str, loss: Union[float, tf.Tensor]):
        """
        Records the model's loss on a dataset for the current global step.

        :param label: str
        :param loss: Union[float, tf.Tensor]
        """
        if tf.is_tensor(loss):
            loss = float(loss.numpy())
        self.history[label].append((self.global_step, loss))

    def plot_history(self, smoothing: float = 0):
        """
        Plots the model's history.

        Since the loss on each mini-batch may be noisy we might want to smoothe
        the loss before plotting to get a better idea of the general trend.
        By setting `smoothing` to a value in the interval [0, 1). When set to
        0, no smoothing is applied. For `loss_smoothing` values closer to 1,
        the loss is exponentially averaged over past mini-batches.

        :param smoothing: float
        """
        fig, ax = plt.subplots(1, 1)
        for label, history in sorted(self.history.items()):
            global_steps, losses = zip(*history)
            losses = exponential_moving_average(losses, beta=smoothing)
            ax.plot(global_steps, losses, label=f'{label.capitalize()} loss')
        ax.legend()
        ax.set_title('Training history')
        path = os.path.join(self.output_dir, 'history.png')
        plt.savefig(path, bbox_inches='tight')
        plt.close(fig)

    @property
    def input_shape(self) -> Union[Tuple[Optional[int], ...],
                                   List[Tuple[Optional[int], ...]]]:
        """
        Returns the input shape of the model as a tuple. If multiple inputs are
        defined, a list of shapes is returned; one for each input.

        :return: Union[Tuple[Optional[int], ...],
                       List[Tuple[Optional[int], ...]]]
        """
        if isinstance(self.keras().input, list):
            return [tuple(t.shape) for t in self.keras().input]
        return tuple(self.keras().input.shape)

    @property
    def output_shape(self) -> Union[Tuple[Optional[int], ...],
                                    List[Tuple[Optional[int], ...]]]:
        """
        Returns the output shape of the model as a tuple. If multiple outputs
        are defined, a list of shapes is returned; one for each output.

        :return: Union[Tuple[Optional[int], ...],
                       List[Tuple[Optional[int], ...]]]
        """
        if isinstance(self.keras().output, list):
            return [tuple(t.shape) for t in self.keras().output]
        return tuple(self.keras().output.shape)

    @contextmanager
    def learning_phase(self, training: bool):
        """
        A convenient context manager for running a particular piece of code in
        either training or inference mode depending on whether `training` is
        True or False, respectively. Code inside this context is run in the
        specified mode. Once the context is closed, the previous mode is
        restored.

        :param training: bool
        """
        was_training = int(self.is_training())
        set_learning_phase(1 if training else 0)
        yield None
        set_learning_phase(was_training)

    @staticmethod
    def is_training() -> bool:
        """
        Returns whether the model is currently running in training mode.

        :return: bool
        """
        phase = learning_phase()
        if isinstance(phase, tf.Tensor):
            session = get_session()
            phase = session.run(phase)
        return bool(phase)

    def on_step(self, interval: int = 1, *args, **kwargs):
        """
        A decorator for triggering a function after every `interval` steps. Any
        `args` or `kwargs` passed along to this method are also passed along to
        the decorated method.

        :param interval: int
        :return: Callable
        """

        def step_decorator(func):
            @wraps(func)
            def step_handler(loss, x, y, predictions):
                if self.global_step % interval == 0:
                    return func(
                        loss, x, y, predictions, *args, **kwargs)

            self.dispatcher.on(ModelEvent.STEP_COMPLETED, step_handler)
            return func

        return step_decorator

    def on_epoch(self, interval: int = 1, *args, **kwargs):
        """
        A decorator for triggering a function after every `interval` epochs.
        Any `args` or `kwargs` passed along to this method are also passed
        along to the decorated method.

        :param interval: int
        :return: Callable
        """

        def epoch_decorator(func):
            @wraps(func)
            def epoch_handler(epoch):
                if epoch % interval == 0:
                    return func(epoch, *args, **kwargs)

            self.dispatcher.on(ModelEvent.EPOCH_COMPLETED, epoch_handler)
            return func

        return epoch_decorator

    def _train_step_callback(
            self,
            loss: tf.Tensor,
            x: Union[tf.Tensor, List[tf.Tensor]],
            y: Union[tf.Tensor, List[tf.Tensor]],
            predictions: Union[tf.Tensor, List[tf.Tensor]],
            validation: Union[Dataset, Dict[str, Dataset], None],
            optimizer: Optimizer
    ):
        """
        A callback that can be called during training to take care of various
        tasks that you would want to perform after a certain number of steps.
        When training a model with `train()`, this method is called after every
        `save_interval` steps, if specified.

        :param loss: tf.Tensor
        :param x: Union[tf.Tensor, List[tf.Tensor]]
        :param y: Union[tf.Tensor, List[tf.Tensor]]
        :param predictions: Union[tf.Tensor, List[tf.Tensor]]
        :param validation: Union[Dataset, Dict[str, Dataset], None]
        :param optimizer: Optimizer
        """
        self.update_history('training', loss)
        if isinstance(validation, Dataset):
            validation = dict(validation=validation)
        if validation:
            for label, validation_set in validation.items():
                validation_loss = self.validate(validation_set, batch_size=1)
                self.update_history(label, validation_loss)
        self.save()
        self.save_optimizer(optimizer)
