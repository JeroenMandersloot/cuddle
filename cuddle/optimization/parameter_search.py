import abc
import os
from typing import Dict, List, Tuple, Union, Optional

import tabulate

Number = Union[int, float]


class ParameterSearch:
    @abc.abstractmethod
    def search(self, *args, **kwargs) -> List[Tuple[Dict[str, Number], float]]:
        """
        Performs the parameter search and returns a list of results, one per
        parameter configuration. Each result is a tuple containing the parameter
        configuration along with the resulting score returned by `self.train()`.

        :return: List[Tuple[Dict[str, Number], float]], the search results
        """
        raise NotImplementedError

    @staticmethod
    def write_results(results: List[Tuple[Dict[str, Number], float]],
                      output_path: Optional[str] = None):
        """
        Prints a result set returned by `self.search()`. If `output_path` is
        specified, it instead writes the results to disk.

        :param results: List[Tuple[Dict[str, Number], float]]
        :param output_path: Optional[str]
        """
        if not results:
            raise ValueError('No results specified')
        params = list(sorted(results[0][0].keys()))
        headers = params + ['Score']
        data = []
        for result in results:
            values = [result[0][param] for param in params]
            score = result[1]
            data.append(values + [score])
        data = reversed(sorted(data, key=lambda row: row[-1]))
        table = tabulate.tabulate(data, headers)
        if output_path:
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
            with open(output_path, 'w') as f:
                f.write(table)
        else:
            print(table)
