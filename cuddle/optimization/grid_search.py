import itertools
from typing import Dict, Callable, List, Tuple

from cuddle.optimization.parameter_search import ParameterSearch, Number


class GridSearch(ParameterSearch):
    def __init__(self,
                 train_func: Callable[..., float],
                 params: Dict[str, List[Number]]):
        """
        Initialize the GridSearch instance.

        :param train_func: Callable[..., float], a callable that takes all the
            configurable parameters as arguments, performs a training loop and
            returns a metric indicating how "good" the parameter values were.
        :param params: Dict[str, List[Number]], a dictionary whose
            keys are the names of the configurable parameters and values are
            lists of corresponding parameter settings to try.
        """
        self.train_func = train_func
        self.params = params

    def train(self, *args, **kwargs) -> float:
        return self.train_func(*args, **kwargs)

    def search(self, *args, **kwargs) -> List[Tuple[Dict[str, Number], float]]:
        results = []
        configs = [[(p, v) for v in s] for p, s in self.params.items()]
        for config in map(dict, itertools.product(*configs)):
            result = self.train_func(*args, **config, **kwargs)
            results.append((config, result))
        return results
