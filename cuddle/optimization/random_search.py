from typing import Dict, Callable, List, Tuple

from cuddle.optimization.parameter_search import ParameterSearch, Number


class RandomSearch(ParameterSearch):
    def __init__(self,
                 num_trials: int,
                 train_func: Callable[..., float],
                 params: Dict[str, Callable[[], Number]]):
        """
        Initialize the RandomSearch instance.

        :param num_trials: int, the number of randomized trials to run.
        :param train_func: Callable[..., float], a callable that takes all the
            configurable parameters as arguments, performs a training loop and
            returns a metric indicating how "good" the parameter values were.
        :param params: Dict[str, Callable[[], Number]], a dictionary
            where they keys make up the names of the configurable parameters and
            their corresponding values are callables that generate a random
            appropriate value for that parameter.
        """
        self.num_trials = num_trials
        self.train_func = train_func
        self.params = params

    def train(self, *args, **kwargs) -> float:
        return self.train_func(*args, **kwargs)

    def search(self, *args, **kwargs) -> List[Tuple[Dict[str, Number], float]]:
        results = []
        for _ in range(self.num_trials):
            params = self.generate_params()
            result = self.train(*args, **params, **kwargs)
            results.append((params, result))
        return results

    def generate_params(self) -> Dict[str, Number]:
        """
        Generate a random value for each configurable parameter.

        :return: Dict[str, Number]
        """
        return {param: dist() for param, dist in self.params.items()}
