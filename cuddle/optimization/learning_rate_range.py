from typing import List, Tuple, Callable

import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.training.optimizer import Optimizer

from cuddle.data import Dataset
from cuddle.models import Model


def learning_rate_range(model: Model,
                        dataset: Dataset,
                        optimizer_func: Callable[[float], Optimizer],
                        output_path: str,
                        min_learning_rate: float = 1e-7,
                        max_learning_rate: float = 10,
                        batch_size: int = 32,
                        step_size: float = 10 ** 0.1):
    losses = []
    learning_rate = min_learning_rate
    batches = dataset.batches(batch_size)
    x, y = next(batches)
    while learning_rate < max_learning_rate:
        optimizer = optimizer_func(learning_rate)
        with tf.GradientTape() as tape:
            predictions = model.predict(x, training=True)
            loss = model.compute_loss(y, predictions)
        variables = model.keras().trainable_variables
        gradients = tape.gradient(loss, variables)
        optimizer.apply_gradients(zip(gradients, variables))
        floss = float(loss.numpy())
        print(f'Learning rate {learning_rate}, loss: {floss}')
        losses.append((learning_rate, floss))
        learning_rate *= step_size
    plot_learning_rate_range(losses, output_path)
    return losses


def plot_learning_rate_range(losses: List[Tuple[float, float]], path: str):
    fig, ax = plt.subplots(1, 1)
    ax.plot(*zip(*losses))
    ax.set_xscale('log')
    ax.set_xlabel('Learning rate')
    ax.set_ylabel('Loss')
    ax.set_title('Learning rate range')
    plt.savefig(path, bbox_inches='tight')
    plt.close(fig)
